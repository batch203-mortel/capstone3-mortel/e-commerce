
import React, {useState, useEffect, useContext} from 'react';
import {Container, Card, Button, Row, Col, Form, Modal} from "react-bootstrap";

import {useParams, useNavigate, NavLink, Navigate } from 'react-router-dom';
import Swal from "sweetalert2";

import UserContext from '../UserContext';

import {motion} from "framer-motion";

export default function ProductView(){
    const [name, setName] = useState('');
    const [description, setDescription] =useState('');
    const [price, setPrice] = useState(0);
    const [stocks, setStocks] = useState(0);
    const [quantity, setQuantity] = useState(0);
 
    const [show, setShow] = useState(false);

    const handleClose = () => setShow(false);
    const handleShow = () => setShow(true);
    const { user } = useContext(UserContext);

    const {productId} = useParams();
    const navigate = useNavigate();

    useEffect(()=>{
        fetch(`${process.env.REACT_APP_API_URL}products/${productId}`)
        .then(response => response.json())
        .then(data => {
            setName(data.productName);
            setDescription(data.description);
            setPrice(data.price);
            setStocks(data.stock);

        })
    }, [productId])

    const order = (productId) => {
    fetch(`${process.env.REACT_APP_API_URL}users/checkout`, {
        method: "POST",
        headers: {
            "Content-Type" : "application/json",
            Authorization: `Bearer ${localStorage.getItem('token')}`
        },
        body : JSON.stringify({
            product: [
                    {
                        productId: productId,
                        productName: name,
                        quantity: quantity
                    }
                    
                ],
            totalAmount: quantity*price   
        })
    })
    .then(response => response.json())
    .then(data => {
      
        if(data){
            if((stocks - quantity) > 0){
                Swal.fire({
                    title: "Successfully placed order!",
                    icon: "success",
                    text: `You have successfully bought ${quantity} pieces of ${name} for ${quantity*price} pesos.`
                   })
                   navigate("/products")
            }
            else{
                Swal.fire({
                    title: "Something went wrong!",
                    icon: "error",
                    text: `We don't have enough stock for this order. We only have ${stocks} as of the moment`
                })
            }
             
        }
        else{
            Swal.fire({
                title: "Something went wrong!",
                icon: "error",
                text: "Please try again!"
            })
        }
    })
    }

    return(
        (user.isAdmin)
        ?
        <Navigate as = {NavLink} to = "/allproducts"/>
        :
        <motion.div initial ={{opacity: 0}} animate={{opacity: 1}} exit = { {opacity: 0}}>
            <Container className='m-1 max-height-100 text-center flex'>
                <Row>
                    <Col>
                    <Card className = "mt-5">
                    
                    <Card.Body>
                        <Card.Title><strong>{name}</strong></Card.Title>
                        <Card.Text>{description}</Card.Text>
                        <Card.Text>{price} pesos</Card.Text>
                        <Card.Text>{stocks} available stocks</Card.Text>
                       
                            
                    </Card.Body>
                    <Card.Footer>
                    {
                           (user.id !== null)
                           ?
                           <Button variant="primary" onClick={handleShow}>
                                 Buy this!
                            </Button>
                            :
                            <Button as = {NavLink} to = "/login"
                            variant="primary" 
                            size = "lg" 
                            >Login
                        </Button>
                        }
                    </Card.Footer>
                     </Card>
                    </Col>
                </Row>
            </Container>

            <Modal show={show} onHide={handleClose}>
        <Modal.Header closeButton>
          <Modal.Title>Checkout Product</Modal.Title>
        </Modal.Header>
        <Modal.Body>
          <Form >
            <Form.Group className="mb-3" controlId="exampleForm.ControlInput1">
              <Form.Label>Enter Quantity</Form.Label>
              <Form.Control
                type="number"
                placeholder="Enter the desired number!"
                value = {quantity}
                onChange = {e => setQuantity(e.target.value)}
                autoFocus
              />
            </Form.Group>
        
          </Form>
        </Modal.Body>
        <Modal.Footer>
          <Button variant="secondary" onClick={handleClose}>
            Cancel
          </Button>
          <Button variant="primary" onClick={() => order(productId)}>
            Checkout
          </Button>
        </Modal.Footer>
      </Modal>

            </motion.div>
    )
}