import {Container, Nav, Navbar} from "react-bootstrap";
import { useContext, useEffect } from "react";
import{ NavLink} from "react-router-dom";
import logo from "../images/logo.png"

import UserContext from "../UserContext";




export default function AppNavBar(){

  const { user} = useContext(UserContext);
  
  useEffect(() =>{}, [user])
    
    return(
       <>
       
        <Navbar collapseOnSelect expand="lg" id = "navBar" variant="dark" sticky="top">
        <Container>
        
          
            <img
              src={logo}
              width="30"
              height="30"
              className="d-inline-block align-top p-1"
              
            />
          
        
          <Navbar.Brand as = {NavLink} to="/" id = "phoebe">Phoebe's Dog Shop</Navbar.Brand>
          <Navbar.Toggle aria-controls="responsive-navbar-nav" />
          <Navbar.Collapse id="responsive-navbar-nav">
           
           
            <Nav className="me-auto">
              <Nav.Link as = {NavLink} to="/">Home</Nav.Link>
              {
                (user.isAdmin)
                ?
                <Nav.Link as= {NavLink} to = "/allproducts">Admin Dashboard</Nav.Link>
                :
                <Nav.Link as = {NavLink} to = "/products">Products</Nav.Link>

              }
            </Nav>
            
            {
              (user.id !== null)
            ?
                (user.isAdmin)
                ?
                  <Nav>
                   
                   <Nav.Link as = {NavLink} to ="/logout" eventKey={2} >Logout: {user.name} {user.lastName}</Nav.Link>
                  </Nav>
                :
                <Nav>
                   <Nav.Link as = {NavLink} to = "/userorders">Orders</Nav.Link>
                   <Nav.Link as = {NavLink} to ="/logout" eventKey={2} >Logout: {user.name} {user.lastName}</Nav.Link>
                  </Nav>

            :
              <Nav>
                <Nav.Link as = {NavLink} to="/login">Login</Nav.Link>
                  <Nav.Link as = {NavLink} to="/register" eventKey={2}>Register</Nav.Link>
              </Nav>
            }
            
            
          </Navbar.Collapse>
        </Container>
      </Navbar>
       </> 
    )
}